package com.devcamp;

import java.util.Date;

public class Visit {
    Customer customer;
    Date date;
    double serviceExpense;
    double productExpense;

    public Visit(Customer customer, Date date){
        this.customer = customer;
        this.date = date;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public String getName() {
        return customer.name;
    }

    public double getTotalExpense(){
        double totalExpense = this.serviceExpense + this.productExpense;
        return totalExpense;
    }

    @Override
    public String toString() {
        return "Visit [customerName=" + customer.name + ", date=" + date + ", productExpense=" + productExpense
                + ", serviceExpense=" + serviceExpense + "]";
    }

    
}
