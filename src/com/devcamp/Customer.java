package com.devcamp;

public class Customer {
    String name;
    boolean member = false;
    String memeberType;

    public Customer(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isMember(){
        this.member = true;
        return this.member;
    }

    public String getMemeberType() {
        return memeberType;
    }

    public void setMember(boolean member) {
        this.member = member;
    }

    public void setMemeberType(String memeberType) {
        this.memeberType = memeberType;
    }

    @Override
    public String toString() {
        return "Customer [member=" + member + ", memeberType=" + memeberType + ", name=" + name + "]";
    }
}
