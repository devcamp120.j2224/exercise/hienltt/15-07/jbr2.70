import java.util.Date;

import com.devcamp.Customer;
import com.devcamp.Visit;

public class App {
    public static void main(String[] args) throws Exception {
        
        Customer customer1 = new Customer("Nguyen Van AA");
        Customer customer2 = new Customer("Tran Van CC");
        System.out.println(customer1);
        System.out.println(customer2);

        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2, new Date());
        System.out.println(visit1);
        System.out.println(visit2);
        System.out.println("Total Expense customer 1: " + visit1.getTotalExpense());
        System.out.println("Total Expense customer 2: " + visit2.getTotalExpense());
    }
}
